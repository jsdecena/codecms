<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Db_calls_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();        
    }

    /*
    *
    * Fetch - Fetch all records in the table
    *
    */
    public function fetch($table, $params = array())
    {
    	$this->db->select($params);
    	$query = $this->db->get($table);

    	if ( $query->num_rows() > 0 ) :
    		return $query->result_array();
    	endif;
    }    

    /*
    *
    * Fetch Where - Fetch a specific column in the database 
    * @params = $params = array($column => $value);
    *
    */
	public function fetch_where($table, $params, $order_by = null, $arrange_by = null, $limit = 10, $offset = 0)
	{
		$this->db->order_by($order_by, $arrange_by);
		$query = $this->db->get_where($table, $params, $limit, $offset);

		if ( $query->num_rows() > 0 ) :
			return $query->result_array();
		endif;
	}

	/*
	* 
	* Query join array
	*
	* 
	*/
	public function query_join($table, $join, $join_id, $table_id, $limit, $offset, $order_by, $arrange_by)
	{		
		$this->db->join($join, "$join.$join_id = $table.$table_id");
		$this->db->order_by($order_by, $arrange_by);
		$query = $this->db->get($table, $limit, $offset);

		if($query->num_rows() > 0):
 			return $query->result_array();
		endif;		
	}

	/*
	* 
	* Query join array single
	*
	* 
	*/
	public function query_join_single($table, $join, $join_id, $table_id, $where, $where_val)
	{
		$this->db->where($where, $where_val);
		$this->db->join($join, "$join.$join_id = $table.$table_id");
		$query = $this->db->get($table);

		if($query->num_rows() > 0):
 			return $query->row();
		endif;		
	}		

	/*
	* 
	* Query a single item
	*
	*/
	public function query_post($table, $column, $where)
	{
		$query = $this->db->get_where($table, array( $column => $where ));

		if($query->num_rows() == 1):
 			return $query->row();
		endif;		
	}	

	/*
	* 
	* Query an array
	* $params = array($column => $value, );
	*
	*/
	public function query_post_array($table, $array = array())
	{
		$query = $this->db->get_where($table, $array );

		if ( $query->num_rows() > 0 ) :
			
			$data = array();
			foreach ($query->result_array() as $value) :
				$data[] = $value;
			endforeach;
			
			return $data;
		endif;		
	}

	/*
	*
	* THIS QUERY IS FOR THE PAGINATION
	*
	*/
	public function query_post_where($table, $order_by, $arrange_by, $limit, $offset, $params = array())
	{
		$this->db->select($params);
		$this->db->order_by($order_by, $arrange_by);
		$query = $this->db->get($table, $limit, $offset);

		if ( $query->num_rows() > 0 ) :
			return $query->result_array();
		endif;
	}

	public function insert($table, $params = array())
	{
		$this->db->insert($table, $params);
	}	

	public function update($table, $column, $where, $params = array())
	{
		$this->db->where($column, $where);
		$this->db->update($table, $params);
	}

	public function delete($table, $params)
	{
		$this->db->delete($table, $params);
	}	

	public function count($table)
	{
		$this->db->count_all($table);
		$this->db->from($table);
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0) :			
			return $query->num_rows();
		endif;
	}

	public function count_where($table, $column, $value)
	{
		$this->db->count_all_results($table);
		$this->db->from($table);
		$this->db->where($column, $value);
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0) :			
			return $query->num_rows();
		endif;
	}

    public function view_post($slug)
    {
        $query = $this->db->get_where('posts', array('slug' => $slug ), 1);
		
		if($query->num_rows() == 1):
 			return $query->row();
		endif;
    }

	//MULTIPLE DELETE
	public function delete_post_selection($selectedIds)
	{
	    $this->db->where_in('post_id', $selectedIds)->delete('posts');
	}
}
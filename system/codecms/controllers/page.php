<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Page extends Front_Controller {

    public $table   = 'posts';
    public $post    = 'post';
    public $page    = 'page';    
        
    function __construct()
    {
        parent::__construct();

    }        

    public function index()
    {
        $data['page']               = $this->db_calls_model->query_post('posts', 'post_id', $this->uri->segment(4)); //THIS IS ACTUALLY RETURNING THE POST WITH A PAGE 'POST TYPE'
        
        $params                     = array('post_type' => 'page', );
        $data['pages']              = $this->db_calls_model->query_post_array('posts', $params);

        //CHECK FOR THE PAGE TO SHOW THE BLOG POSTS ELSE SHOW REGULAR PAGE
        if ( isset($data['page']->post_id) && $data['page']->post_id == $this->posts_model->get_setting('POST_PAGE_CHOSEN') ) :
            
            $data['posts']      = $this->db_calls_model->fetch('post', '*');
            
            $this->template->title = "Blog page";
            $this->template->content->view('templates/public/default/contents/posts', $data);
            $this->template->publish();
        else:

            $this->template->title = isset($data['page']->title) ? $data['page']->title : "Page Not Found";
            $this->template->content->view('templates/public/default/contents/pages', $data);
            $this->template->publish();
        endif;
    }

    public function view()
    {
        $this->template->set_template('templates/public/default/blogs');

        $params                     = array('post_type' => 'page', );
        $data['pages']              = $this->db_calls_model->query_post_array('posts', $params);
        $data['page']               = $this->db_calls_model->query_post('posts', 'slug', $this->uri->segment(1));

        $blog                       = $this->settings_model->get_setting('POST_PAGE_CHOSEN');
        
        if ( $blog == $this->uri->segment(1) ) :
            $post                       = array(
                                                'post_type' => $this->post,
                                                'status'    => 'published'
                                            );
            $data['posts']              = $this->db_calls_model->query_post_array($this->table, $post); //GET ALL THE POSTS
        endif;        

        $this->template->title      = @$data['page']->title;

        $this->template->content->view('templates/public/default/contents/blog', $data);
        $this->template->publish();
    }
        
}
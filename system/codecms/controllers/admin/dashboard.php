<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Dashboard extends Admin_Controller {


    function __construct(){

        parent::__construct();
    }    

	public function index(){

        //CHECK FIRST IF THE USER IS ALREADY LOGGED IN
        if ( $this->session->userdata('is_logged_in')) :

            $this->template->title      = 'Dashboard';
            $data['logged_info']        = $this->users_model->logged_in();            
            $this->template->content->view('templates/admin/default/dashboard', $data);
            
            // publish the template
            $this->template->publish();

            $this->users_model->insert_identity();
        else:
          redirect('admin');
        endif;
	}
}
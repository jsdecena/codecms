<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Users extends Admin_Controller {

    public $table  = 'users';
    public $column = 'users_id';

    public function index()
    {
        //CHECK IF YOU ARE ADMIN
        if ( $this->session->userdata('role') == 'admin' ) :

            $this->template->title  = 'User list page';               

            $data['user_data']      = $this->users_model->users();
            $data['logged_info']    = $this->users_model->logged_in();

            $this->template->content->view('templates/admin/default/users/index', $data);
            $this->template->publish();
        else:
            $data['error'] = $this->session->set_flashdata('error', 'Hey, do not have this authority to do this.');
            redirect('admin', $data); 
        endif;
    }

    public function create()
    {
        //CHECK IF YOU ARE ADMIN
        if ( $this->session->userdata('role') == 'admin' ) :

            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|is_unique[users.username]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|sha1');
            $this->form_validation->set_rules('role','Role','required|callback_check_default');

            //CUSTOM MESSAGE FOR THE EMAIL THAT ALREADY EXIST IN THE DATABASE
            $this->form_validation->set_message('is_unique', 'The %s you are creating already exists. Please try again.');

            //CUSTOM MESSAGE FOR THE ROLE TO BE CHOSEN FIRST.
            $this->form_validation->set_message('check_default', 'You must select role for the user.');

            if ( $this->form_validation->run() === TRUE ) :

                $params = array(
                    'username'      => $this->input->post('username'),
                    'first_name'    => $this->input->post('first_name'),
                    'last_name'     => $this->input->post('last_name'),
                    'email'         => $this->input->post('email'),
                    'role'          => $this->input->post('role'),
                    'is_logged_in'  => 0,
                    'identity'      => 0,
                    'password'      => $this->input->post('password')
                );

                $this->db_calls_model->insert($this->table, $params);

                $data['success']    = $this->session->set_flashdata('create_success', 'You have successfully created a user.');
                $data['error']      = $this->session->set_flashdata('create_error', 'Sorry, we have a problem creating a user.');
                redirect('admin/users/create', $data);
            endif;            

            $this->template->title  = 'User creation page';

            $data['logged_info']    = $this->users_model->logged_in();

            $this->template->content->view('templates/admin/default/users/create', $data);
            
            // publish the template
            $this->template->publish();

        else:
            $data['error'] = $this->session->set_flashdata('error', 'Hey, do not have this authority to do this.');
            redirect('admin', $data); 
        endif;
    }

    /* ------- UPDATE USER LIST PAGE ----------- */  

    public function update()
    {
        $data['data']           = $this->users_model->user_query();
        $data['logged_info']    = $this->users_model->logged_in();

        //CHECK IF YOU ARE ADMIN
        if ( $this->session->userdata('role') == 'admin' ) :
            
            if ( $this->input->post('save')) :

                $this->form_validation->set_rules('about','About','trim|xss_clean');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');           
                $this->form_validation->set_rules('password', 'Password', 'sha1');                
                
                //USER ID
                $id = $this->input->post('users_id');
                
                if ( $this->form_validation->run() ) :

                    //IF THE PASSWORD IS LEFT BLANK
                    if ($this->input->post('password')) :
                        $params = array(
                            'password'          => $this->input->post('password')
                        );
                    endif;

                        $params = array(
                            'first_name'        => $this->input->post('first_name'),
                            'last_name'         => $this->input->post('last_name'),
                            'email'             => $this->input->post('email'),
                            'role'              => $this->input->post('role')
                        );                     

                    //UPDATE THE USER IN THE DB 
                    $this->db_calls_model->update($this->table, $this->column, $this->input->post('users_id'), $params);
                endif;
                
                $data['success']    = $this->session->set_flashdata('success', 'Update successful.');
                redirect("admin/users/update/$id", $data );                
            endif; //IF POST SAVE     

            $this->template->title  = 'User update page';
            $this->template->content->view('templates/admin/default/users/update', $data);
            $this->template->publish();

        else:
            $data['error'] = $this->session->set_flashdata('error', 'Hey, do not have this authority to do this.');
            redirect('admin', $data); 
        endif;               
    }

    public function delete()
    {
        //CHECK IF YOU ARE ADMIN
        if ( $this->session->userdata('role') == 'admin' ) :

            $logged_user = $this->users_model->logged_in();

            //PREVENT DELETING YOUR OWN ACCOUNT
            if ( $this->uri->segment(4) !=  (int)$logged_user['users_id']) :
            
                $this->users_model->delete_user();

                $data['success']    = $this->session->set_flashdata('success', 'You have successfully deleted a user.');                
                redirect('admin/users', $data);
            else:
                $data['error']    = $this->session->set_flashdata('error', 'Sorry, you cannot delete your own account.');                
                redirect('admin/users', $data);
            endif;
        else:
            $data['error'] = $this->session->set_flashdata('error', 'Hey, do not have this authority to do this.');
            redirect('admin', $data); 
        endif;
    }
}
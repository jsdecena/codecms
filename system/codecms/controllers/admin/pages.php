<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Pages extends Admin_Controller {

    public $table       = 'posts';
    public $column      = 'post_type';
    public $column_key  = 'page';

	public function index()
    {
        if ( $this->session->userdata('role') == 'admin' ) :

            $this->load->library('pagination');

            $config['base_url']         = base_url(''.$this->uri->segment(1) .'/'. $this->uri->segment(2));
            $config['total_rows']       = $this->db_calls_model->count_where($this->table, $this->column, $this->column_key);
            $config['per_page']         = $this->limit;
            $config['full_tag_open']    = '<div class="pagination"><ul>';
            $config['full_tag_close']   = '</ul></div>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';   
            $config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['prev_tag_open']    = '<li id="prev_item">';
            $config['prev_tag_close']   = '</li>';
            $config['next_tag_open']    = '<li id="next_item">';
            $config['next_tag_close']   = '</li>';
            $config['first_tag_open']   = '<li id="first">';
            $config['first_tag_close']  = '</li>';            
            $config['last_tag_open']    = '<li id="last">';
            $config['last_tag_close']   = '</li>';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';

            $this->pagination->initialize($config);

            $data['links']              = $this->pagination->create_links();            
            $data['logged_info']        = $this->users_model->logged_in();
            
            $offset                     = $this->uri->segment(3);
            $params                     = array('post_type' => 'page');
            $data['page']               = $this->db_calls_model->fetch_where($this->table, $params, 'position', 'ASC', $config['per_page'], $offset);
            
            $this->template->title      = 'Pages Listing';
            $this->template->content->view('templates/admin/default/pages/page', $data);
            
            // publish the template
            $this->template->publish();
        else:
            redirect('admin');
        endif;
	}

    public function create($post_type = 'page')
    {
        
        if ( $this->session->userdata('role') == 'admin' ) :

            if ( $this->input->post('create')) :

                $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
                $this->form_validation->set_rules('content', 'Content', 'required|xss_clean');

                if ( $this->form_validation->run() ) :

                    if ( $this->input->post('create') ) :
                        
                        $query = $this->db->get_where('users', array( 'email' => $this->session->userdata('email') ));
                        $author = $query->row();        

                        $params = array(
                            'post_type'     => $this->input->post('post_type'),
                            'users_id'      => $author->users_id,
                            'title'         => $this->input->post('title'),
                            'content'       => $this->input->post('content'),
                            'slug'          => strtolower(url_title($this->input->post('title'))),
                            'author'        => $author->first_name ." ". $author->last_name,
                            'status'        => $this->input->post('status'),
                            'post_parent'   => $this->input->post('post_parent'),
                            'post_cat'      => $this->input->post('post_cat'),
                            'date_add'      => date("Y-m-d H:i:s")
                        );

                        $this->db_calls_model->insert($this->table, $params);
                        $data['success'] = $this->session->set_flashdata('success', 'You have successfully created a page.');              

                        redirect("admin/pages", $data);
                    endif;
                endif;
            endif;

            $this->template->title      = 'Create a Page';

            $data['logged_info']        = $this->users_model->logged_in();
            $params                     = array('post_type' => 'page', );
            $data['pages']              = $this->db_calls_model->query_post_array('posts', $params);
            
            $this->template->content->view('templates/admin/default/pages/create', $data);
            
            // publish the template
            $this->template->publish();
        else:
            redirect('admin');
        endif;                    
    }

    public function edit()
    {
        
        if ( $this->session->userdata('role') == 'admin' ) :

            if ( $this->input->post('edit')) :

                $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
                $this->form_validation->set_rules('content', 'Content', 'trim|xss_clean');

                if ( $this->form_validation->run() ) :

                    //VALIDATION SUCCCESS
                    $params = array(
                        'title'     => $this->input->post('title'),
                        'content'   => $this->input->post('content'),
                        'status'    => $this->input->post('status'),                            
                        'post_cat'  => $this->input->post('post_cat'),
                        'slug'      => strtolower(url_title($this->input->post('title'))),
                    );

                    $this->db_calls_model->update($this->table, 'post_id', $this->input->post('post_id'), $params);
                    
                    $postID = $this->input->post('post_id');
                    $data['success'] = $this->session->set_flashdata('success', 'You have successfully edited this page.');
                    redirect("admin/pages/edit/$postID", $data);

                else:

                    //VALIDATION FAILURE
                    $data['error'] = $this->session->set_flashdata('error', 'Sorry, Title is required.');
                    redirect("admin/pages/edit/$postID", $data);

                endif;            

            endif;
            
            $this->template->title      = 'Pages';
            $post_id                    = $this->uri->segment(4);            

            $data['logged_info']        = $this->users_model->logged_in();
            $data['page']               = $this->db_calls_model->query_post('posts', 'post_id', $this->uri->segment(4)); //THIS IS ACTUALLY RETURNING THE POST WITH A PAGE 'POST TYPE'
            
            $params                     = array('post_type' => 'page', );
            $data['pages']              = $this->db_calls_model->query_post_array('posts', $params);
            
            $this->template->content->view('templates/admin/default/pages/edit', $data);
            
            // publish the template
            $this->template->publish();
        else:
            redirect('admin');
        endif;            
    }

    public function quick_update()
    {
        $data = array(
            'status' => $this->input->post('status'),
            'post_type' => $this->input->post('post_type')
        );
        
        $this->db_calls_model->update('posts', 'post_id', $this->input->post('post_id'), $data);
        
        if ( $this->input->post('status') == 'published') :
            $data['success']    = $this->session->set_flashdata('success', 'You have successfully PUBLISHED a page.');
        else:
            $data['error']    = $this->session->set_flashdata('error', 'You have successfully UNPUBLISHED a page.');
        endif;                

        redirect('admin/pages', $data);
    }

    public function delete()
    {
        if ( $this->session->userdata('role') == 'admin' ) :

            $params = array(
                'post_type' => 'page',
                'post_id' => $this->uri->segment(4)
            );

            $this->db_calls_model->delete('posts', $params );
            $data['success']    = $this->session->set_flashdata('success', 'Deleted successfully.');

            redirect('admin/pages', $data);
        else:
            redirect('admin');
        endif;
    }

    public function delete_selection()
    {
        $selectedIds = $_POST['selected'];
        $this->db_calls_model->delete_post_selection($selectedIds);         

    }

    public function sort()
    {
        foreach ($_GET['row'] as $key => $item) :
            $this->db->set('position', $key);
            $this->db->where('post_id', $item);
            $this->db->where($this->column, $this->column_key);
            $this->db->update($this->table);
        endforeach;

        echo "You have switched position.";
    }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

define('THEME_DIR', 'templates/admin/');

class Categories extends Admin_Controller {    

    public $table   = 'categories';
    public $column  = 'cat_id';

    public function index()
    {
        $this->template->title      = 'Category Listing';
        $data['logged_info']        = $this->users_model->logged_in();
        $data['categories']         = $this->db_calls_model->fetch('categories', '*');
        $this->template->content->view('templates/admin/default/categories/index', $data);
        $this->template->publish();
    }

    public function create()
    {
        $data['logged_info']        = $this->users_model->logged_in();
        
        if ( $this->session->userdata('role') == 'admin' ) :        
                
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

            if ( $this->form_validation->run() ) :

                if ( $this->input->post('cat_create') ) :

                    $data = array('name' => $this->input->post('name') );                 

                    $this->db_calls_model->insert('categories', $data);
                    $data['success'] = $this->session->set_flashdata('success', 'You have successfully created a category.');
                    redirect("admin/categories", $data);
                endif;
            endif;
        else:
            $data['error']    = $this->session->set_flashdata('error', 'Sorry, you are not authorized to do this action.');
            redirect('admin/categories', $data);
        endif;            

        $this->template->title      = 'Post Categories';
        $this->template->content->view('templates/admin/default/categories/create', $data);
        $this->template->publish();
    }

    public function edit()
    {
        $data['logged_info']        = $this->users_model->logged_in();

        $data['category']           = $this->db_calls_model->query_post($this->table, $this->column, $this->uri->segment(4));
        
        if ( $this->session->userdata('role') == 'admin' ) :        
                
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

            if ( $this->form_validation->run() ) :

                if ( $this->input->post('cat_edit') ) :
                    
                    $params = array('name' => $this->input->post('name') );

                    $this->db_calls_model->update($this->table, 'cat_id', $this->input->post('cat_id'), $params);
                    $data['success'] = $this->session->set_flashdata('success', 'You have successfully created a category.');
                    redirect("admin/categories", $data);
                endif;
            endif;
        else:
            $data['error']    = $this->session->set_flashdata('error', 'Sorry, you are not authorized to do this action.');
            redirect('admin/categories', $data);
        endif;            

        $this->template->title      = 'Post Categories';
        $this->template->content->view('templates/admin/default/categories/edit', $data);
        $this->template->publish();
    }

    public function delete()
    {
        if ( $this->session->userdata('role') == 'admin' ) :

            $params = array(
                'cat_id' => $this->uri->segment(4)
            );

            $this->db_calls_model->delete('categories', $params );
            $data['success']    = $this->session->set_flashdata('success', 'You have deleted a category.');

            redirect('admin/categories', $data);
        else:
            redirect('admin');
        endif;
    }            
}
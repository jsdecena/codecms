<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Index extends Front_Controller 
{
    public $table   = 'posts';
    public $page    = 'page';
    public $post    = 'post';

    function __construct(){

        parent::__construct();
    }

    public function index()
    {

        //DISPLAY THE POSTS IN THE HOMEPAGE IF NOTHING IS SET IN THE DATABASE. ( NO SET PAGE TO SHOW POST )
        $define_page                    = (bool)$this->settings_model->get_setting('POST_PAGE_CHOSEN');

        //IF THERE IS NO PAGE DEFINE TO SHOW THE BLOG POSTS, SHOW IT IN THE INDEX PAGE.
        if ($define_page === false) :
            $post                       = array(
                                                'post_type' => $this->post,
                                                'status'    => 'published'
                                            );
            $data['posts']              = $this->db_calls_model->query_post_array($this->table, $post); //GET ALL THE POSTS
        endif;
        
        $page                           = array(
                                                'post_type' => $this->page,
                                                'status'    => 'published'
                                            );
        $data['pages']                  = $this->db_calls_model->query_post_array($this->table, $page); //GET ALL THE PAGES 

        $this->template->title          = 'Codecms - Home';
        $this->template->content->view('templates/public/default/contents/index', $data);
        $this->template->publish();                   
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */

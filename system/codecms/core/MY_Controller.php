<?php

class MY_Controller extends CI_Controller
{
	public $table;
    public $column_key;
    public $upload_dir;
	public $admin_assets_path 		= 'templates/admin/';
	public $public_assets_path      = 'templates/public/';
    public $limit;
    public $order_by;
    public $arrange_by;
    public $logged;
    public $id_user;	

    function __construct()
    { 
    	parent::__construct();

        $this->upload_dir = __UPLOAD_DIR__;

        $this->limit                        = $this->settings_model->get_setting('POST_PER_PAGE');      //SETTINGS PER PAGE VALUE  
        $this->order_by                     = $this->settings_model->get_setting('ARRANGE_POST_BY');   //SETTINGS POST BY "post_id" or "date"
        $this->arrange_by                   = $this->settings_model->get_setting('ORDER_POST_BY');     //ARRANGE BY DESC OR ASC
        
        $info 								= $this->users_model->logged_in();
        $this->logged 						= $info['is_logged_in'];
        $this->id_user                      = $info['users_id'];
    }    
}
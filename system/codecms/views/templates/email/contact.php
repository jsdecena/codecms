<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $reason; ?></title>
        <style type="text/css">
            @media only screen and (max-width: 480px){
                #templateColumns{
                    width:100% !important;
                }

                .templateColumnContainer{
                    display:block !important;
                    width:100% !important;
                }

                .columnImage{
                    height:auto !important;
                    max-width:480px !important;
                    width:100% !important;
                }

                .leftColumnContent{
                    font-size:16px !important;
                    line-height:125% !important;
                }

                .rightColumnContent{
                    font-size:16px !important;
                    line-height:125% !important;
                }
            }
        </style>    
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateColumns">
            <tr>
                <td align="center" valign="top">
                    <table align="left" border="0" cellpadding="10" cellspacing="0" width="300" class="templateColumnContainer">
                        <tr>
                            <td class="leftColumnContent">
                                <img src="http://ibabyphotography.com/templates/public/default/bootstrap3/images/logo.jpg" width="280" style="max-width:280px;" class="columnImage" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="leftColumnContent">
                                <h4>Someone has sent an Inquiry:</h4>
                                First Name: <?php echo $first_name ?> <br />
                                Last Name: <?php echo $last_name ?> <br />
                                Email: <?php echo $email ?> <br />
                                Mobile: <?php echo $phone ?> <br />
                                Website: <?php echo $website ?> <br />
                                Message: <?php echo $message ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
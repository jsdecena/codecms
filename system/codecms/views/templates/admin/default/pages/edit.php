<div class="row clearfix:after pages_list">
	
	<div class="span3 bs-docs-sidebar">

		<!-- LOAD THE PAGE SIDEBAR -->
		<?php $this->load->view('templates/admin/default/shared/sidebar'); ?>
		
	</div>

	<div class="span9">
		<section id="pages">
			<div class="control-group">

				<!-- LOAD THE PAGE CONTENT -->
				<?php 

				//PAGE EDIT FORM

				$attr = array('class' => 'clear', 'id' => 'page_edit');
				echo form_open('admin/pages/edit', $attr); ?>

				<div class="controls clearfix">
					<input type="hidden" id="page_slug" class="input-block-level" name="slug" value="<?php echo $page->slug; ?>">
					<input type="hidden" class="input-block-level" name="post_id" value="<?php echo $page->post_id; ?>">
					<label for="page_title">Page Title <sup class="text-error">*</sup></label>
					<input type="text" id="page_title" class="input-block-level" name="title" value="<?php echo $page->title; ?>">
				</div>

				<div class="controls clearfix">
					<textarea name="content" id="content" class="input-block-level ckeditor" cols="30" rows="10"><?php echo $page->content; ?></textarea>
				</div>


				<div id="page_attributes" class="controls">
					<h5>Page Attributes</h5>
					<div class="control-group">
						<label class="page_status control-label">Status:</label>
						<div class="controls">							
							<select name="status">
								<option value="unpublished" <?php if ( $page->status == 'unpublished') : ?>selected="selected"<?php endif; ?>>Unpublished</option>
								<option value="published" <?php if ( $page->status == 'published') : ?>selected="selected"<?php endif; ?>>Published</option>
							</select>					
						</div>												
					</div>

					<div class="control-group">
						<label class="page_parent control-label">Page Parent:</label>
						<div class="controls">
							<select name="page_parent">
								<option value="0">No Parent</option>
								<?php foreach ($pages as $page) : if ( $page['post_id'] != $this->uri->segment(4) && $page['status'] != 'unpublished' && $page['post_parent'] == 0) : //DO NOT INCLUDE THE CURRENT PAGE & UNPUBLISHED PAGE/s ON THE CHOICES ?>									
									<option value="<?php echo $page['post_id']; ?>"><?php echo $page['title']; ?></option>
								<?php endif; endforeach;?>								
							</select>					
						</div>												
					</div>									
				</div>				

				<div class="controls">
					<a href="<?php echo base_url('admin/pages'); ?>" class="btn btn-info">Back</a>
					<input type="submit" name="edit" class="btn btn-primary" value="Save" />
				</div>

				<?php echo form_close(); ?>
			</div>
		</section>
	</div>	
</div>
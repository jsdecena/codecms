<div class="row clearfix:after pages_list">
	<div class="span3 bs-docs-sidebar"><?php $this->load->view('templates/admin/default/shared/sidebar'); ?></div>

	<div class="span9">
		<section id="pages">
			<div class="control-group">
				
				<a class="btn btn-primary" href="<?php echo base_url('admin/pages/create'); ?>">New page</a>
				<?php if ( isset($page) && is_array($page)) : ?>
					<p class="alert alert-success" style="display:none" id="ajax-response"></p>
					<table class="table table-striped" id="sortable" data-link="<?php echo base_url($this->uri->segment(1) ."/". $this->uri->segment(2)); ?>">
						<thead>
							<tr>
								<th><input type="checkbox" id="select_all" /></th>
								<th class="tbl_title">Title</th>
								<th class="tbl_content hidden-phone">Content</th>
								<th class="tbl_author hidden-phone">Author</th>
								<th class="tbl_actions">Actions</th>
								<th>Position</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($page as $post) : $pageID = $post["post_id"]; $slug = $post["slug"]; ?>
								<?php if ( $post['post_parent'] == 0 ) : //PARENT PAGES ?>
									<tr id="row_<?php echo $post["post_id"]; ?>">
										<td><input class="delete_selection" type="checkbox" name="delete_selection" value="<?php echo $post['post_id']; ?>" /></td>
										<td class="tbl_title">
											<a title="Edit this page" href="<?php echo base_url("admin/pages/edit/$pageID"); ?>"><?php echo $post['title']; ?></a>
										</td>
										<td class="tbl_content hidden-phone"><?php echo word_limiter($post['content'], 5); ?></td>
										<td class="tbl_author hidden-phone"><?php echo $post['author']; ?></td>
										<td class="tbl_actions">
										
										<?php echo anchor( "$slug", '<i class="icon-search icon-white">&nbsp;</i>', 'target="_blank" class="btn btn-info btn-small"'); ?>
										
										<?php if ( $logged_info['role'] == 'admin' ) : ?>

										<a class="btn btn-small btn-primary" href="<?php echo base_url("admin/pages/edit/$pageID") ?>"><i class="icon-pencil icon-white"></i></a>
										<?php echo anchor("admin/pages/delete/$pageID", '<i class="icon-trash icon-white">&nbsp;</i>', 'class="btn btn-danger btn-small"', 'onClick="return confirm("Are you sure you want to delete?")"'); ?>
										<?php echo form_open('admin/pages/quick_update'); if ( $post['status'] == 'unpublished') : ?>
											
											<!-- ACTION TO QUICK PUBLISH THE PAGE-->
											<input type="hidden" name="post_id" value="<?php echo $post["post_id"]; ?>" />
											<input type="hidden" name="post_type" value="<?php echo $post["post_type"]; ?>" />
											<button name="status" class="btn btn-danger" value="published" onClick="return confirm('Publish this page?')">
											<i class="icon-remove icon-white icon_status">&nbsp;</i>

										<?php else: ?>

											<!-- ACTION TO QUICK UNPUBLISH THE PAGE-->
											<input type="hidden" name="post_id" value="<?php echo $post["post_id"]; ?>" />
											<input type="hidden" name="post_type" value="<?php echo $post["post_type"]; ?>" />
											<button name="status" class="btn btn-success" value="unpublished" onClick="return confirm('Unpublish this page?')">
											<i class="icon-ok icon-white icon_status">&nbsp;</i>
																					
										<?php endif; echo form_close(); ?>
										<?php endif; ?>
										</td>
										<td><i class="icon-drag icon-handle"></i></td>
									</tr>
								<?php endif; ?>

								<?php 
										$params = array('post_parent' => $pageID, );
										$results = $this->db_calls_model->query_post_array('posts', $params); // CHILD PAGES ?>
										
								<?php if( !empty($results)) : ?>
									<?php foreach ($results as $result) : $childID = $result['post_id']; $slug = $result["slug"];  ?>
										<?php if ( isset($result['post_parent']) || $page['post_parent'] != 0  || $result['post_parent'] == $pageID  ) : ?>
											<tr class="tbl_page_child">
												<td><input class="delete_selection" type="checkbox" name="delete_selection" value="<?php echo $result['post_id']; ?>" /></td>											
												<td><a title="Edit This Page" href="<?php echo base_url("admin/pages/edit/$childID"); ?>">-- <?php echo $result['title']; ?></a>
												</td>
												<td><?php echo $result['content']; ?> </td>
												<td><?php echo $result['author']; ?></td>
												<td class="tbl_actions">
												<?php echo anchor( "$slug", '<i class="icon-search icon-white">&nbsp;</i>', 'target="_blank" class="btn btn-info btn-small"'); ?>
												
												<?php if ( $logged_info['role'] == 'admin' ) : ?>
												
												<?php echo anchor("admin/pages/edit/$childID", '<i class="icon-pencil icon-white">&nbsp;</i>', 'class="btn btn-primary btn-small"'); ?>
												<?php echo anchor("admin/pages/delete/$childID", '<i class="icon-trash icon-white">&nbsp;</i>', 'class="btn btn-danger btn-small"', 'onClick="return confirm("Are you sure you want to delete?")"'); ?>
												<?php echo form_open('admin/pages/quick_update'); if ( $result['status'] == 'unpublished') : ?>
													
													<!-- ACTION TO QUICK PUBLISH THE PAGE-->
													<input type="hidden" name="post_id" value="<?php echo $childID; ?>" />
													<input type="hidden" name="post_type" value="<?php echo $result["post_type"]; ?>" />
													<button name="status" class="btn btn-danger" value="published" onClick="return confirm('Publish this page?')">
														<i class="icon-remove icon-white icon_status">&nbsp;</i>													
													</button>

												<?php else: ?>

													<!-- ACTION TO QUICK UNPUBLISH THE PAGE-->
													<input type="hidden" name="post_id" value="<?php echo $childID; ?>" />
													<input type="hidden" name="post_type" value="<?php if(isset($page["post_type"])) : echo $page["post_type"]; endif; ?>" />
													<button name="status" class="btn btn-success" value="unpublished" onClick="return confirm('Unpublish this page?')">
														<i class="icon-ok icon-white icon_status">&nbsp;</i>
													</button>
																							
												<?php endif; echo form_close(); ?>
												<?php endif; ?>											
												</td>
											</tr>
										<?php endif; ?>	
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endforeach; ?>	
						</tbody>
					</table>

					<div class="pull-left action_left">
						<button data-url="<?php echo base_url('admin/pages/delete_selection'); ?>" id="delete_selection" name="delete_selection" class="btn btn-danger btn-small" value="" onClick="return confirm("Are you sure you want to delete?")"><i class="icon-trash icon-white"> &nbsp; </i> Delete Selected</button>
					</div>

					<div class="pull-right action_right">
						<?php echo $links; //PAGINATION ?>
					</div>
					
				<?php else: ?>

				  <div class="text-error alert-block alert-error fade in">
				    <p>Ooops, No more pages!</p>
				  </div>

				<?php endif; ?>
			</div>
		</section>
	</div>	

</div>
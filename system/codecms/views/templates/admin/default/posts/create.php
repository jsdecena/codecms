<div class="row clearfix:after pages_list">
	<div class="span3 bs-docs-sidebar"><?php $this->load->view('templates/admin/default/shared/sidebar'); ?></div>
	<div class="span9">
		<section id="pages">
			<div class="control-group">

				<?php

				//PAGE CREATION FORM

				$attr = array('id' => 'post_create');
				echo form_open('admin/posts/create', $attr); ?>

				<div class="controls clearfix">
					<input type="hidden" id="post" name="post_type" value="post" />
					<label for="post_title">Post Title <sup class="text-error">*</sup></label>
					<input type="text" class="input-block-level" id="post_title" name="title" value="<?php echo $this->input->post('title'); ?>">
					<input type="hidden" class="input-block-level" id="post_slug" name="slug" value="">				
				</div>

				<div class="controls clearfix">
					<textarea name="content" id="content" class="input-block-level ckeditor" cols="30" rows="10"><?php echo $this->input->post('content'); ?></textarea>
				</div>

				<div class="controls">
					<label>Publishing options:</label>
					<div class="control-group clearfix">
						<select name="status">
							<option value="unpublished" selected="selected">Unpublished</option>
							<option value="published">Published</option>
						</select>
					</div>					
				</div>

				<div class="controls">
					<label>Categories</label>
					<div class="control-group clearfix">
						<select name="post_cat">
							<option value="0">Select Category</option>
							<?php foreach ($categories as $category ) : ?>
							<option value="<?php echo $category['cat_id'] ?>"> <?php echo $category['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>				

				<div class="controls">
					<a href="<?php echo base_url('admin/posts'); ?>" class="btn btn-info">Go Back</a>
					<input type="submit" name="post_create" class="btn btn-primary" value="Create a post" />
				</div>
				<?php echo form_close(); ?>
			</div>
		</section>
	</div>	
</div>
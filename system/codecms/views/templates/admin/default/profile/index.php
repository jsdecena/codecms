<?php $this->load->view('templates/admin/default/header'); ?>

<div class="row clearfix:after">
	
	<div class="span3 bs-docs-sidebar">
	
	<?php $this->load->view('templates/admin/default/profile/sidebar');?>
		
	</div>

	<div class="span9">
		
		<?php echo $this->template->content; ?>

	</div>	

</div>

<?php $this->load->view('templates/admin/default/footer'); ?>
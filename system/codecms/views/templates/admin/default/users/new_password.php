	<?php 

	$attr = array('class' => 'form-signin', 'id' => 'myform');
	  echo form_open('new_password', $attr);
	?>

	<h2 class="form-signin-heading">New Password</h2>
    
    <?php if ( $this->session->flashdata('success') ) : ?>
      <div class="alert alert-success fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <?php echo $this->session->flashdata('success'); ?> 
      </div>
    <?php elseif ( $this->session->flashdata('error') ): ?>
      <div class="alert alert-error fade in"> 
        <a class="close" data-dismiss="alert">&times;</a>
        <?php echo $this->session->flashdata('error'); ?> 
      </div>
    <?php endif; ?>	
	
	<?php if ( validation_errors() ) :  ?>
		<div class="text-error"> <?php echo validation_errors(); ?> </div>
	<?php endif; ?>

	<input type="hidden" name="key" value="<?php echo $this->uri->segment(2); ?>">
	
	<div class="control-group">		
		<div class="control">
			<input type="password" class="input-block-level" name="password" placeholder="Enter new password" value="">
		</div>
	</div>

	<button class="btn btn-sml btn-primary" name="new_password">Create New Password</button>
	</form>
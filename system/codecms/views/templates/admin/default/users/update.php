<div class="row users_update">

  <div class="span3 sidebar">
  
    <?php  $this->load->view('templates/admin/default/users/sidebar');?>
    
  </div>  

  <div class="span9">
  
    <section class="update_details">    
      
      <div class="controlgroup">
      
      <?php if ( isset($data) || is_array($data)) : ?>
      
        <div class="page-header"> Edit user details </div>          

      <?php
          $attr = array('class' => 'form-signin', 'id' => 'create_user_form');
          echo form_open('admin/users/update', $attr);
      ?>

        <?php foreach ($data as $user_data): ?>
        
          <input type="hidden" value="<?php echo $user_data->users_id; ?>" name="users_id">
          <label for="username" class="muted">Username</label>
          <input type="text" class="input-block-level" name="username" value="<?php echo $user_data->username; ?>" disabled />

          <label for="first_name" class="muted">First Name</label>
          <input type="text" class="input-block-level" name="first_name" value="<?php echo $user_data->first_name; ?>" />
          
          <label for="last_name" class="muted">Last Name</label>
          <input type="text" class="input-block-level" name="last_name" value="<?php echo $user_data->last_name; ?>" />
          
          <label for="email" class="muted">Email <sup class="text-error">*</sup></label>
          <input type="text" class="input-block-level" name="email" value="<?php echo $user_data->email; ?>" />

          <label for="password" class="muted">Password </label>
          <input type="password" class="input-block-level" name="password" value="" />
          <span class="text-error">Leave password blank if you are not changing it.</span>
          
          <?php if ( $logged_info['role'] == 'admin') : ?>
          <br /> <br />
            <label for="role" class="muted">User Role <sup class="text-error">*</sup></label>
            <select name="role" id="role">
              <option value="admin" <?php if ( $user_data->role == 'admin' ): echo "selected=\"selected\""; endif; ?>>Admin</option>
              <option value="subscriber" <?php if ( $user_data->role == 'subscriber' ): echo "selected=\"selected\""; endif; ?>>Subscriber</option>
            </select>
          
          <?php endif; ?>
        <?php endforeach; ?>
        <br class="clear" />
        <a href="<?php echo base_url('admin/users'); ?>" class="btn btn-info">Go Back</a>
        <input type="submit" name="save" class="btn btn-primary" value="Update details" />        
      <?php endif; ?>
    
      <?php echo form_close(); ?>      

    </div>

  </section>


</div>

</div> <!-- /row -->
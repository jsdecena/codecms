<?php if ( isset($user_data) && is_array($user_data) && count($user_data) ) : ?> 
<div class="row">

	<div class="span3 sidebar">
		
		<?php echo $this->load->view('templates/admin/default/users/sidebar'); ?>

	</div>

	<div class="span9">

		<div class="page-header"> Users List </div>			
		
		<table class="table table-striped">
			<thead>
			<tr>
				<th>ID</th>
				<th class="hidden-phone">Username</th>
				<th class="hidden-phone">First Name</th> 
				<th class="hidden-phone">Last Name</th>
				<th>Email</th>
				<th class="hidden-phone">Role</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>

			<?php foreach($user_data as $data): $id = $data['users_id']; ?>
				<tr>
					<td><?php echo $data['users_id']; ?></td>
					<td class="hidden-phone"><?php echo $data['username']; ?></td>
					<td class="hidden-phone"><?php echo $data['first_name']; ?></td>
					<td class="hidden-phone"><?php echo $data['last_name']; ?></td>
					<td><?php echo $data['email']; ?></td>
					<td class="hidden-phone"><?php echo $data['role']; ?></td>
					<td>
						<?php echo anchor("admin/users/update/$id", '<i class="icon-pencil icon-white">&nbsp;</i>', 'class="btn btn-primary btn-small"'); ?>
						<?php echo anchor("admin/users/delete/$id", '<i class="icon-trash icon-white">&nbsp;</i>', 'class="btn btn-danger btn-small" onClick="return confirm(\'Delete this user?\')"'); ?>
					</td>
				</tr>
			<?php endforeach; ?>  	
			</tbody>
		</table>
	</div>	

</div> <!-- END ROW -->

<?php endif; ?>
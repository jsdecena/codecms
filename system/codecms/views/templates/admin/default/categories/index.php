<div class="row clearfix:after posts_list">
	
	<div class="span3 bs-docs-sidebar"><?php $this->load->view('templates/admin/default/shared/sidebar'); ?></div>

	<div class="span9">
		<section id="pages">
			<div class="controlgroup">
				
				<div id="ajax_message" class="text-success alert-block alert-success fade in" style="display:none"></div>
				<a class="btn btn-primary" href="<?php echo base_url('admin/categories/create'); ?>">Create Category</a>
				<?php 

				//POST LIST FORM

				if ( is_array($categories)) : ?>				
				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th class="tbl_id">ID</th>
							<th class="tbl_title">Name</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($categories as $category) : $cat_id = $category["cat_id"]; ?>
							<tr class="row_<?php echo $category["cat_id"]; ?>">								
								<td class="tbl_id"><?php echo $category['cat_id']; ?></td>
								<td class="tbl_title"><?php echo $category['name']; ?></td>
								<td>
								<?php if ( $logged_info['role'] == 'admin' ) : ?>
								<!-- ACTION TO EDIT THE PAGE-->
								<?php echo anchor("admin/categories/edit/$cat_id", '<i class="icon-pencil icon-white">&nbsp;</i>', 'class="btn btn-small btn-primary"'); ?>
								<?php echo anchor("admin/categories/delete/$cat_id", '<i class="icon-trash icon-white"> &nbsp; </i>', 'class="btn btn-small btn-danger"', 'onClick="return confirm("Are you sure you want to delete?")'); ?>
																			
								<?php echo form_close(); ?>
								<?php endif; ?>
								</td>
							</tr>			
						<?php endforeach; ?>
					</tbody>
				</table>				
				
				<div class="pull-left action_left">
					<button id="delete_selection" name="delete_selection" class="btn btn-danger btn-small" value=""><i class="icon-trash icon-white"> &nbsp; </i> Delete Selected</button>
				</div>

				<div class="pull-right action_right">
					<?php // echo $links; //PAGINATION ?>
				</div>
				
				<?php else: ?>

				  <div class="text-error alert-block alert-error fade in">
				  	<a class="close" data-dismiss="alert">&times;</a>
				    <p>Ooops, No more categories!</p>				    
				  </div>

				<?php endif; ?>
			</div>
		</section>
	</div>	
</div>
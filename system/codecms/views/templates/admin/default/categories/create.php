<div class="row clearfix:after pages_list">
	
	<div class="span3 bs-docs-sidebar"><?php $this->load->view('templates/admin/default/shared/sidebar'); ?></div>

	<div class="span9">
		<section id="pages">
			<div class="controlgroup">

				<?php

				//PAGE CREATION FORM

				$attr = array('id' => 'cat_create');
				echo form_open('admin/categories/create', $attr); ?>

				<div class="controls clearfix">
					<label for="cat_title">Category Title <sup class="text-error">*</sup></label>
					<input type="text" class="input-block-level" id="cat_title" name="name" value="<?php echo $this->input->post('title'); ?>">
				</div>

				<div class="controls">
					<a href="<?php echo base_url('admin/categories'); ?>" class="btn btn-info">Go Back</a>
					<input type="submit" name="cat_create" class="btn btn-primary" value="Create a category" />
				</div>
				<?php echo form_close(); ?>
			</div>
		</section>
	</div>
</div>
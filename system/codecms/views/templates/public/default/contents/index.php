      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>CodeCMS</h1>
        <p>This is a project by Jeff Simons Decena aimed to be an alternative CMS solution made from Philippines!</p>
        <p>
          <a class="btn btn-lg btn-primary" href="https://bitbucket.org/jsdecena/codecms/get/8a78a4f37f42.zip" role="button">Download now &raquo;</a>
        </p>
      </div>
	
	<?php if ( isset($posts) && $posts ) : ?>
		<?php foreach ($posts as $post) : $slug = $post['slug']; ?>
			<div class="row blog-row">
				<div class="col-md-4">
				  <h2><?php echo $post['title']; ?></h2>
				  <?php echo $post['content']; ?>
				  <p><a class="btn btn-default" href="<?php echo base_url("$slug"); ?>" role="button">View details &raquo;</a></p>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
##How To install / Customization

1. Create a database - 'codecms' . You can change this in the system/config/database.php
2. Dump the sql in your newly created database. The SQL dump is found in <root folder>/templates/admin/default/sql/db.sql
3. Change the setting of your database in <root folder>/system/codecms/config/database.php; Make sure your DB User and DB Pw match on your local setting.
4. Login url for the admin area is at http://yourwebsite.com/admin
5. Default user is "John Doe". 
-- Login details are as follows: 
Email: admin@admin.com
Password: 123123123
